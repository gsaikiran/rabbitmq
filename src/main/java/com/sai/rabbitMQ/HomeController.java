package com.sai.rabbitMQ;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

/**
 * Most basic example of rabbit mq.
 * A producer is a user application that sends messages.
 * A queue is a buffer that stores messages.
 * A consumer is a user application that receives messages.
 * 
 * in this basic example , RabbitMQ just dispatches a message when the message enters the queue
 * It just blindly dispatches every n-th message to the n-th consumer.
 */
@Controller
public class HomeController {
	private int i = 0;
	private final static String QUEUE_NAME = "myQueue";
	@RequestMapping(value = "/producer", method = RequestMethod.GET)
	public String home() {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();//all messaging is done through a channel
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			String message = "Hello World! "+i++;
			channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
			System.out.println("Producer Sent '" + message + "'");
			channel.close();
			connection.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		} 
		return "";
	}
	
	@RequestMapping(value="/consumer/{name}",method = RequestMethod.GET)
	public void consumer1(@PathVariable("name") String name
			,@RequestParam(value="delay",required = false,defaultValue="2000") int delay){
		consumer("consumer "+name,delay);
		
	}
    
	private void consumer(final String name,final int delay){
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection = factory.newConnection();
			final Channel channel = connection.createChannel();
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			System.out.println(name + " is waiting for messages....");
		
			Consumer consumer = new DefaultConsumer(channel) {
			  @Override
			  public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			      throws IOException {
				    String message = new String(body, "UTF-8");
				    System.out.println("Received '" + message + "'  by "+name);			
					try {
						Thread.sleep(delay);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			  }
			};
			channel.basicConsume(QUEUE_NAME,true, consumer);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
